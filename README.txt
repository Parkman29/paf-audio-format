# PAF Audio Format

## License:
This project is licensed under the GPL3

## Aim:
This projects aim is to make a compact & minimalistic audio format that is designed to be easy to transfer, but is not meant to be used for professional distribution as their's no extra stuff like the author and track in the file.

## File Architecture:

### Examples:
Filename: example.paf
File header (First couple of bytes):

* All of these are unsigned integers of either 8 Bit, 16 Bit, 32 Bit, or 64 Bits

Hex:        | FF FF FF FF | FF | FF FF FF FF FF FF FF FF | 73 74 72 74 | 57 6F 77 20 79 6F 75 20 66 6F 75 6E 64 20 6D 79 20 6C 69 74 74 6C 65 20 73 65 63 72 65 74 |
                   |        |               |                   |                                                    |
Meaning:        Bitrate  Channels         Length              Start                                                 Data
Unit:             Hz      0-255             Ms            Start of data              Lossless compression is a goal but i'm not quite sure how to do it
